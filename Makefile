SCRIPTS ?= abs aur checkrestart diffrepo fetchpkg makemetapkg metapkg pacred \
           pkgmirror remakepkg repkg zr
LIBS ?= basedir environment error makepkg message package pacman version
MANPAGES ?= diffrepo fetchpkg makemetapkg metapkg pkgmirror remakepkg repkg zr

# Version:
VERSION = 0.12.0
PATCHES = $(shell git rev-list --count "v${VERSION}"..HEAD)
UNIXDATE = $(shell git show --pretty=format:'%ct' -s HEAD)
VERSIONDATE = $(shell date --date='@${UNIXDATE}' --rfc-3339=seconds)
MANDATE = $(shell date --date='@${UNIXDATE}' '+%Y-%m-%d')
ifneq (${PATCHES}, 0)
	VERSIONSTR = v${VERSION}p${PATCHES}
else
	VERSIONSTR = v${VERSION}
endif
INFOSTR = ${VERSIONSTR} (${VERSIONDATE})
PACMAN_CONFIG ?= /etc/pacman.conf

# Sources:
SRCDIR_SCRIPTS = src
SRCDIR_LIBS = ${SRCDIR_SCRIPTS}/lib
SRCDIR_MANPAGES = man
SRC_SCRIPTS = $(SCRIPTS:%=${SRCDIR_SCRIPTS}/%)
SRC_LIBS = $(LIBS:%=${SRCDIR_LIBS}/%.sh)
SRC_MANPAGES = $(MANPAGES:%=${SRCDIR_MANPAGES}/%.1)

# Build:
BUILDDIR ?= build
BUILDDIR_SCRIPTS = ${BUILDDIR}/bin
BUILDDIR_LIBS = ${BUILDDIR}/libs
BUILDDIR_MANPAGES = ${BUILDDIR}/manpages
BUILD_SCRIPTS = $(SRC_SCRIPTS:${SRCDIR_SCRIPTS}/%=${BUILDDIR_SCRIPTS}/%)
BUILD_LIBS = $(SRC_LIBS:${SRCDIR_LIBS}/%=${BUILDDIR_LIBS}/%)
BUILD_MANPAGES = $(SRC_MANPAGES:${SRCDIR_MANPAGES}/%=${BUILDDIR_MANPAGES}/%)

# Install:
PREFIX ?= /usr/local
PREFIX_BIN ?= ${PREFIX}/bin
PREFIX_LIB ?= ${PREFIX}/lib
PREFIX_MAN ?= ${PREFIX}/share/man
DESTDIR ?=
DESTDIR_BIN ?= ${DESTDIR}${PREFIX_BIN}
DESTDIR_LIB ?= ${DESTDIR}${PREFIX_LIB}
DESTDIR_MAN ?= ${DESTDIR}${PREFIX_MAN}
DEST_BIN = $(BUILD_SCRIPTS:${BUILDDIR_SCRIPTS}/%=${DESTDIR_BIN}/%)
DEST_LIB = $(BUILD_LIBS:${BUILDDIR_LIBS}/%=${DESTDIR_LIB}/pacman-hacks/%)
DEST_MAN = $(BUILD_MANPAGES:${BUILDDIR_MANPAGES}/%=${DESTDIR_MAN}/man1/%)

# ==============================================================================

.PHONY: all
all: build_scripts build_libs build_manpages

# Build directory:
${BUILDDIR}:
	mkdir '${BUILDDIR}'
${BUILDDIR_SCRIPTS}: | ${BUILDDIR}
	mkdir '${BUILDDIR_SCRIPTS}'
${BUILDDIR_LIBS}: | ${BUILDDIR}
	mkdir '${BUILDDIR_LIBS}'
${BUILDDIR_MANPAGES}: | ${BUILDDIR}
	mkdir '${BUILDDIR_MANPAGES}'


.PHONY: clean
clean:
	rm -rf '${BUILDDIR}'

.PHONY: build_scripts
build_scripts: ${BUILD_SCRIPTS}
${BUILDDIR_SCRIPTS}/%: ${SRCDIR_SCRIPTS}/% | ${BUILDDIR_SCRIPTS}
	sed -e "s|^\\(readonly PHLIBDIR=\\).*\$$|\\1'${PREFIX_LIB}/pacman-hacks'|g" '$<' >'$@'

.PHONY: build_libs
build_libs: ${BUILD_LIBS}
${BUILDDIR_LIBS}/version.sh: ${SRCDIR_LIBS}/version.sh Makefile | ${BUILDDIR_LIBS}
	sed -e "s|^\\(readonly PHVER=\\).*\$$|\\1'${INFOSTR}'|g" '$<' >'$@'
${BUILDDIR_LIBS}/pacman.sh: ${SRCDIR_LIBS}/pacman.sh | ${BUILDDIR_LIBS}
	sed -e "s|^\\(readonly PACMAN_CONFIG=\\).*\$$|\\1'${PACMAN_CONFIG}'|g" '$<' >'$@'
${BUILDDIR_LIBS}/%: ${SRCDIR_LIBS}/% | ${BUILDDIR_LIBS}
	cp '$<' '$@'

.PHONY: build_manpages
build_manpages: ${BUILD_MANPAGES}
${BUILDDIR_MANPAGES}/%.1: man/%.1 Makefile | ${BUILDDIR_MANPAGES}
	sed -e 's|@@DATE@@|${MANDATE}|;s|@@VERSION@@|${VERSIONSTR}|' '$<' >'$@'

# ==============================================================================

.PHONY: install install_bin install_lib install_man
install: install_bin install_lib install_man

.PHONY: install_bin
install_bin: ${DEST_BIN}
${DESTDIR_BIN}/%: ${BUILDDIR_SCRIPTS}/%
	install -Dm 0755 '$<' '$@'

.PHONY: install_lib
install_lib: ${DEST_LIB}
${DESTDIR_LIB}/pacman-hacks/%: ${BUILDDIR_LIBS}/%
	install -Dm 0644 '$<' '$@'

.PHONY: install_man
install_man: ${DEST_MAN}
${DESTDIR_MAN}/man1/%.1: ${BUILDDIR_MANPAGES}/%.1
	install -Dm 0644 '$<' '$@'
