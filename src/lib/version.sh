if [ -z "${_PH_VERSION_SH_:-}" ]; then readonly _PH_VERSION_SH_=1

readonly PHVER= # overwritten by Makefile
readonly PHCMD=$(basename "$0")

print_version()
{
	cat <<- EOF
	$PHCMD (pacman-hacks) $PHVER
	Copyright (C) 2017-2019 Tinu Weber <http://ayekat.ch>

	Licenced under the GNU General Public License, version 3.
	See https://www.gnu.org/licenses/ for more information.
	EOF
}

fi
