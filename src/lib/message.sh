if [ -z "${_PH_MESSAGE_SH_:-}" ]; then readonly _PH_MESSAGE_SH_=1

. "$PHLIBDIR"/error.sh

readonly MSG_DEBUG=7
readonly MSG_INFO=4
readonly MSG_SUCCESS=3
readonly MSG_WARN=2
readonly MSG_ERROR=1
readonly MSG_QUIET=0

MSG_LEVEL=$MSG_INFO
set_msg_level()
{
	case "$1" in
		(*[!0-9]*) die $E_INTERNAL 'non-numeric message level: %s' "$1" ;;
		($MSG_DEBUG|$MSG_INFO|$MSG_SUCCESS|$MSG_WARN|$MSG_ERROR|$MSG_QUIET) ;;
		(*) die $E_INTERNAL 'invalid message level: %d' $1 ;;
	esac
	MSG_LEVEL=$1
}

debug() { phmsg $MSG_DEBUG "$@"; }
info() { phmsg $MSG_INFO "$@"; }
success() { phmsg $MSG_SUCCESS "$@"; }
warn() { phmsg $MSG_WARN "$@"; }
error() { phmsg $MSG_ERROR "$@"; }

phmsg()
{
	test $1 -le $MSG_LEVEL || return $E_SUCCESS
	case $1 in
		($MSG_ERROR) printf '\033[31m>>>\033[0m ' >&2 ;;
		($MSG_WARN) printf '\033[33m>>>\033[0m ' >&2 ;;
		($MSG_SUCCESS) printf '\033[32m>>>\033[0m ' >&2 ;;
		($MSG_INFO) printf '\033[34m>>>\033[0m ' >&2 ;;
		($MSG_DEBUG) printf '\033[35m>>>\033[0m ' >&2 ;;
	esac
	shift
	printf "$@" >&2; echo >&2
}

print_deprecate()
{
	warn 'DEPRECATION WARNING'
	cat >&2 <<- EOF
	    $1 is deprecated and may be unavailable in future releases.
	    $2 should be used instead.
	EOF
}

fi
