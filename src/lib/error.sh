if [ -z "${_PH_ERROR_SH_:-}" ]; then readonly _PH_ERROR_SH_=1

. "$PHLIBDIR"/message.sh

readonly E_SUCCESS=0
readonly E_USER=1
readonly E_CONFIG=2
readonly E_FILESYSTEM=3
readonly E_NETWORK=4
readonly E_EXEC=5
readonly E_PACKAGE=6
readonly E_INTERNAL=13

strerror()
{
	case "$1" in
		($E_SUCCESS) printf 'E_SUCCESS' ;;
		($E_USER) printf 'E_USER' ;;
		($E_CONFIG) printf 'E_CONFIG' ;;
		($E_FILESYSTEM) printf 'E_FILESYSTEM' ;;
		($E_NETWORK) printf 'E_NETWORK' ;;
		($E_EXEC) printf 'E_EXEC' ;;
		($E_PACKAGE) printf 'E_PACKAGE' ;;
		($E_INTERNAL|*) printf 'E_INTERNAL' ;;
	esac
}

die()
{
	retval=$(($1)); shift
	if [ $# -gt 0 ]; then
		format=$1; shift
		if [ $retval -eq $E_USER ]; then
			error "$format" "$@"
			info 'Run with `-h` for help'
		else
			error "%s: $format" "$(strerror $retval)" "$@"
		fi
	else
		error '%s' "$(strerror $retval)"
	fi
	exit $retval
}

fi
