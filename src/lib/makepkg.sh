if [ -z "${_PH_MAKEPKG_SH_:-}" ]; then readonly _PH_MAKEPKG_SH_=1

readonly MAKEPKG_CONFIG=/etc/makepkg.conf # overwritten by Makefile

makepkg_read_variable()
(
	set -eu

	read_variable()
	{
		if [ -e "$1" ]; then
			bash -c "source \"$1\" && echo \"\${$2:-}\""
		fi
	}

	variable=$(read_variable "$XDG_CONFIG_HOME"/pacman/makepkg.conf "$1")
	variable=${variable:-$(read_variable "$MAKEPKG_CONFIG" "$1")}
	echo "$variable"
)

fi
